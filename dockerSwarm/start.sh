#!/bin/bash

echo "In first, we install the infrastructure"
ansible-playbook playbook.yml >> resultTP
echo "Your infrastructure is ready to run"

echo" Now, work and configure the Docker's Manager"
ansible-playbook master.yml >> resultTP
echo "Your Docker's Manager is up"

echo "Configure your Docker's Worker"
ansible-playbook worker.yml -i hosts2.ini >> resultTP
echo "Your Docker's Worker is Up"

echo "Start the service"
ansible-playbook service.yml >> resultTP
echo "You can check your ERP on the firewall's ip !"
