# docker-LFT

Bienvenue sur le repository dédié à Docker et Docker-Compose

## Contexte

Dans ce repository vous trouverez tout les documents nécessaires à la mise en place d'une infrastructure sous Docker et Docker-Compose, pour l'entreprise "La Force Tranquille" (projet TP), qui souhaite migrer son application Odoo vers un environnement basé sur Docker.
Jusqu'au dernier chapitre (7/), 2 commandes sont nécessaires, afin de déployer entièrement l'infrastructure. La première et l'éxécution d'un fichier (`azure.sh`) bash permettant l'installation et la mise en place de toute l'infrastructure. La Seconde, s'éxécute à l'intérieur d'une machine virtuelle et éxécute un playbook ansible. 

## Composition du Repository

Vous trouverez plusieurs fichiers et dossiers dans ce Repository, dont vous trouverez le détail et leurs contenu en suivant

#### 1/ Création de l'infrastructure `azure.sh`

Ce script à pour but :

- La Création du Vnet sur Azure
- La création des sous-réseau
- La création du Groupe de sécurité et des rêgles entrantes
- La création de la VM qui gérera les contenurs

Ce script permet la mise à jour du système de la Machine Virtuelle et le parametrage de la  VM "Master" pour l'utilisatin de Docker (qui est dejà installé sur l'image de la VM choisi). Il installe également Ansible afin d'automatiser chaque processus Docker.

#### 2/ Deux conteneurs Docker exécutés à partir d'une image publique.

Dans le dossier "pullImages", vous trouverez le playbook Ansible permettant le pull de 2 Images du Docker Hub (PostGres et Odoo) ainsi que leurs lancements sur 2 conteneurs.

- `ansible.cfg` : Paramètres de base pour le fonctionnement d'Ansible
- `hosts.ini` : Inventaire des hôtes 
- `vars/main.yml` : Fichier Yaml gérant les variables
- `playbook.yml` : Ce playbook me permet de pull les images "Odoo" et "Postgres" du Docker Hub et monte les conteneurs. Il afficher également le status des conteneurs lancés.
- `resultTP` : Enregistrement de la sortie du `playbook.yml`

Après avoir installer l'infra, connectez-vous à la vm puis  lancer le playbook : `ansible-playbook playbook.yml`

#### 3/ Une image Docker personnalisée, construite à partir d'un Dockerfile

Dans le dossier "personnalDocker" :

- `ansible.cfg` : Paramètre de base pour le fonctionnement d'Ansible
- `hosts.ini` : Inventaire des hôtes
- `vars/main.yml` : Fichier yaml gérant les variables
- `playbook.yml` : Ce playbook va dans un premier temps récupérer le fichier `Dockerfile` afin de pouvoir construire l'image souhaité. Ensuite, il lance le conteneur. Pour finir il pull une image et lance un deuxièmre conteneur.
- `resultTP` : Enregistrmeent du résultat du `playbook.yml`
- `Dockerfile`: Fichier de configuration pour la construction du de l'image 'postgres'

Après avoir installer l'infra, connectez-vous à la vm puis  lancer le playbook : `ansible-playbook playbook.yml`

#### 4/ Un réseau Docker créé et testé pour la communication entre conteneurs

Il existe différents `network` que Docker peut utiliser. Ces `network` appelé `bridge` permettent aux conteneurs de communiquer entre eux et d'accéder à un réseau externe pour certains dont voici les différents types:

- `driver none` : Ce type de driver interdit toute communication interne et externe.
- `driver host` : Il permet aux conteneurs d'utiliser la même interface réseau que l'hôte. Il supprime donc l'isolation réseau entre les conteneurs et seront accessible de l'éxtérieur via l'adresse IP de l hôte.
- `driver overlay` : Il permet la communication des conteneurs installés sur des hôtes différents.
- `driver bridge` : Si aucun driver n'est spécifié, c'est celui-ci qui sera paramétré de base. Il permet, au sein d'un hôte unique, aux conteneurs de communiquer entre eux. Si nous souhaitons faire comuniquer nos conteneurs vers l'extérieur nous devons mettre en place un mappage de port.

Etant donné que nous sommes actuellement sur un même hôte pour les conteneurs "LFT", nous partirons sur le `driver bridge`

###### Quelques commandes `docker network`

- `docker network create --driver bridge myBridge` : créé un réseau bridge appelé "myBridge"
- `docker run -dit --name db --network myBridge postgre` & `docker run -dit -p 80:8069 --name odoo --network myBridge odoo` : Ici nous venons pull une image postgre et lançons un conteneur nommé "db" que nous lions au réseau "myBridge" que nous avons créé juste avant. Nous faisons ensuite la même chose avec une image "Odoo" et venons mapper le port 8069 du conteneur au port 80 de l'hote afin que nous puissions accéder à l'ERP.
- `docker network inspect myBridge` : permet d'inspecter notre réseau.
- `docker network disconnect myBridge db`: Cette commande permet de déconnecter le conteneur "db" du réseau "myBridge"

#### 5/ Un volume Docker monté et testé pour la persistance des données.

Dans le dossier `volDocker` :

- `ansible.cfg` : Paramètre de base pour le fonctionnement d'Ansible
- `hosts.ini` : Inventairedes hôtes
- `vars/main.yml` : Fichier yaml gérant les variables
- `playbook.yml` : Ce playbook permet la Cration d'un volume puis le pull and run d'un conteneur "Postgres" lié au volume précédemment créé. Il Créé également un conteneur "Odoo". Ensuite il le playbook créé un fichier nommé `toto.txt` sur notre hôte et y insert du texte à l'intérieur. Ce fichier est copié à l'intérieur du conteneur dans le répertoire lié au volume.Il montre le contenu du répertoire de notre volume sur l'hôte et nous voyons bien que le fichier `toto.txt` y apparaît bien. Pour finir il supprime le conteneur "db" puis en créé un nouveau avec la même liaison de volume précédent. Il exécute la commande `ls -l` dans le répertoirelié au volume dans le conteneur. Le répertoire `toto.txt` est bien présent.
- `resultTP` : Enregistrement du résultat du `playbook.yml.`

#### 6/ Un fichier de configuration Docker-Compose décrivant une application multi-conteneurs.

Dans le dossier `compose` :


- `ansible.cfg` : Paramètre de base pour le fonctionnement d'Ansible
- `hosts.ini` : Inventaire des hôtes
- `vars/main.yml` : Fichier yaml gérant les variables
- `docker-compose.yml` : Vous trouverez dans ce Fichier, le script nécessaire au paramétrage et à l'intstallation des conteneurs Odoo et Postgres
- `playbook.yml` : Ce playbook, permet de lancer le fichier `docker-compose.yml` afin de pouvoir construire notre application multi-conteneur.
- `resultTP` : Enregistrement du résultat du `playbook.yml.`

Après avoir installer l'infra, connectez-vous à la vm puis  lancer le playbook : `ansible-playbook playbook.yml`

#### 7/ Une infrastructure Sécurisée déployant cette même infrastructure.

Afin d'automatiser tout le processus d'installation en un seul script, je suis parti sur un seul playbook Ansible.
Ce playbook install : 

- Un groupe de sécurité et ses règles
- Un Vnet
- Un sous réseau dédié au Firewall
- Un sous réseau dédié à la Machine virtuelle
- Une ipPublique utile au Firewall
- Une interface réseau utile à la Machine Virtuelle
- Une Machine Virtuelle sous Rocky Linux
- Un Firewall et ses règles "D-NAT"
- Se connecte à la VM en ssh
- Pull et buil des images Docker Postgresql et Odoo.

Voici la composition du dossier `finalInfra` :

- `ansible.cfg` : Paramètre de base pour le fonctionnement d'Ansible
- `hosts.ini` : Inventaire des hôtes
- `vars/main.yml` : Fichier yaml gérant les variables
- `docker-compose.yml` : Vous trouverez dans ce Fichier, le script nécessaire au paramétrage et à l'intstallation des conteneurs Odoo et Postgres
- `playbook.yml` : Ce playbook, permet de mettre en place l'infrastructure et lance le  fichier `docker-compose.yml` afin de pouvoir construire notre application multi-conteneur.
- `resultTP` : Enregistrement du résultat du `playbook.yml.`
- `infra`: Schéma de mon infrastructure.

Lancer la commande `ansible-playbook playbook.yml` pour lancer le script.
Une fois le playbook finalisé, vous pourrez accéder en ssh à la VM en passant par l'ip du firewall et a l'ERP Odoo

#### 8/ Docker Swarm (Petit bonus après rendu)

Dans le dossier `dockerSwarm` :
J'ai réalisé différents playbook afin de déployer l'infrastructure suivante:
(Pour info, j'ai rencontré des difficultés lors du mappage ssh sur un même inventaire. Pour palier à cela, j'ai créé différents playbook (4) et construis un fichier éxécutbale bash les appelant dans un ordre bien précis, afin de ne devoir lancé qu'un seul script) 

- Groupes de sécurité
- Réseau et sous réseau
- Firewall avec règles Dnat afin de pouvoir accéder à 2 Machines Virtuelles sur des réseaux différents
- 2 Machines virtuelles sur des sous réseaux différents
- Connection ssh aux 2 Vms en passant obligatoirement par le firewall
- Déploiement d'un Cluster "Docker Swarm" avec un Manager et un worker
- Conteneur Docker Postgresql sur l'une et Conteneur Docker Odoo sur l'autre (imposé par le `docker-compose.yml`)

Afin de lancer la mise en place de cette insfrastructure: `./start.sh`

