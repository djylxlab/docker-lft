# Création de mon réseau Vnet sur Azure

# Déclaration des variables

MyIP=92.175.74.250
vnetName=Vnet_OCC_ASD_Jacques
resourceGroup=OCC_ASD_JACQUES
networkAddressPrefix="10.0.11.0/24"
subnetName="Subnet_Vnet_OCC_ASD_Jacques_"
subnetAddressPrefix="10.0.11."
i=1
x=8
subnetNetmask="/29"
admin="admin"
nsg=NSG_OCC_ASD_JACQUES
portNameHttp=OdooHttp
portNameDb=OdooDb
portHttp=8069
portDb=5444

# Création d'un groupe de sécurité

az network nsg create \
    --resource-group $resourceGroup \
    --name $nsg

# Assignation des règles du Groupe de Sécurité

az network nsg rule create \
    --resource-group $resourceGroup \
    --nsg-name $nsg \
    --name $portNameHttp \
    --priority 110 \
    --destination-address-prefixes 'VirtualNetwork' \
    --destination-port-ranges 80 \
    --protocol Tcp \
    --description "Allow HTTP"

az network nsg rule create \
    --resource-group $resourceGroup \
    --nsg-name $nsg \
    --name SSH-rule \
    --priority 100 \
    --source-address-prefixes $MyIP \
    --destination-address-prefixes VirtualNetwork \
    --destination-port-ranges 22 \
    --protocol Tcp \
    --description "Allow ssh"

# Création d'un réseau et du sous réseau admin

az network vnet create \
    --name $vnetName \
    --resource-group $resourceGroup \
    --network-security-group $nsg \
    --address-prefix $networkAddressPrefix \
    --subnet-name $subnetName$admin \
    --subnet-prefix $subnetAddressPrefix"0"$subnetNetmask

# Création des autres sous-réseaux

while [ $i -ne 8 ]
do
  az network vnet subnet create \
      --name $subnetName$i \
      --resource-group $resourceGroup \
      --vnet-name $vnetName \
      --address-prefix $subnetAddressPrefix$x$subnetNetmask \
      --network-security-group $nsg
  x=$((x+8))
  i=$((i+1))
done

#Création de la machine virtuelle Master et de la node-db

subnets=$(az network vnet subnet list --resource-group $resourceGroup --vnet-name $vnetName --query "[].id" -o tsv)

for subnet in $subnets; do
    subnet_name=$(basename $subnet)
    if [ "$subnet_name" = "$subnetName$admin" ]; then
        az vm create \
            --resource-group $resourceGroup \
            --location francecentral \
            --vnet-name $vnetName \
            --image tidalmediainc:docker-compose-rocky-9:docker-compose-rocky-9:1.0.0 \
            --nsg $nsg \
            --admin-user jacques \
            --ssh-key-value /home/jacques/.ssh/id_rsa.pub \
            --name Master \
            --subnet $subnet_name \
            --size Standard_B2s \
            --public-ip-address publicip 
    elif [ "$subnet_name" = "$subnetName"2 ]; then
        az vm create \
            --resource-group $resourceGroup \
            --location francecentral \
            --vnet-name $vnetName \
            --image tidalmediainc:docker-compose-rocky-9:docker-compose-rocky-9:1.0.0 \
            --nsg $nsg \
            --admin-user jacques \
            --ssh-key-value /home/jacques/.ssh/id_rsa.pub \
            --name Node-db \
            --subnet $subnet_name \
            --size Standard_B1s \
            --public-ip-address ""
   fi
done

# Met à jour et paramètre la VM "Master" pour utiliser Docker et Ansible
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name Master --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo dnf update -y && sudo dnf install epel-release -y && sudo dnf install ansible -y && sudo usermod -aG docker jacques && sudo reboot"}'
